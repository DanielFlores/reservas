CREATE DATABASE  IF NOT EXISTS `reservaumss` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `reservaumss`;
-- MySQL dump 10.13  Distrib 8.0.34, for Win64 (x86_64)
--
-- Host: localhost    Database: reservaumss
-- ------------------------------------------------------
-- Server version	8.0.34

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `aula`
--

DROP TABLE IF EXISTS `aula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `aula` (
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `capacidad` int DEFAULT NULL,
  `habilitado` tinyint(1) DEFAULT NULL,
  `tipo_aula` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`nombre`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `aula`
--

LOCK TABLES `aula` WRITE;
/*!40000 ALTER TABLE `aula` DISABLE KEYS */;
INSERT INTO `aula` VALUES ('611D','es un aula tipo auditorio',200,0,'auditorio'),('650A','auditorio pequeño',25,1,'auditorio'),('650B','es un auditorio pequeño',50,1,'auditorio'),('650C','auditorio mediano',100,1,'auditorio'),('650D','auditorio grande',150,1,'auditorio'),('660A','es un auditorio pequeño',25,1,'auditorio'),('670A','aula comun pequeña',25,1,'aula-comun'),('670B','es un aula pequeña aula comun',50,1,'aula-comun'),('670C','es un aula comun mediana',100,1,'aula-comun'),('670D','es un aula comun grande',150,1,'aula-comun'),('680A','es un aula grande',50,1,'laboratorio'),('680B','es un aula pequeña',25,1,'laboratorio'),('680C','es un aula extremadamente grande',100,1,'laboratorio'),('680D','es un aula del tamaño del auditorio y es laboratorio',150,1,'laboratorio'),('691F','aula real y ultimo',100,0,NULL),('692A','aula Grande',150,0,NULL),('692F','aula real',100,1,NULL),('693A','aula',150,0,NULL),('693Fa','sdasd',150,1,NULL),('693G','asdasd',233,0,NULL),('694A','es una aula',200,0,NULL),('694B','es una aula',200,0,NULL),('695A','aula',150,1,NULL),('696A','aula',300,1,NULL),('697A','aula',300,1,NULL),('699A','aulaaaa',122,1,NULL),('699D','asdads',222,1,NULL),('699F','sasdasdasd',50,0,'laboratorio'),('888B','es para ver que todo este bien',400,0,'auditorio'),('asdasd','es una aula',1,1,NULL),('NombreEjemplo','DescripciónEjemplo',50,1,NULL),('NombreEjemplo1','DescripciónEjemplo',50,1,NULL),('NombreEjemplo12','DescripciónEjemplo',50,1,NULL);
/*!40000 ALTER TABLE `aula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `periodo`
--

DROP TABLE IF EXISTS `periodo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `periodo` (
  `id` int NOT NULL AUTO_INCREMENT,
  `h_inicio` time DEFAULT NULL,
  `h_final` time DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `periodo`
--

LOCK TABLES `periodo` WRITE;
/*!40000 ALTER TABLE `periodo` DISABLE KEYS */;
INSERT INTO `periodo` VALUES (1,'06:45:00','07:30:00'),(2,'07:30:00','08:15:00'),(3,'08:15:00','09:00:00'),(4,'09:00:00','09:45:00'),(5,'09:45:00','10:30:00'),(6,'10:30:00','11:15:00'),(7,'11:15:00','12:00:00'),(8,'12:00:00','12:45:00'),(9,'12:45:00','13:30:00'),(10,'13:30:00','14:15:00'),(11,'14:15:00','15:00:00'),(12,'15:00:00','15:45:00'),(13,'15:45:00','16:30:00'),(14,'16:30:00','17:15:00'),(15,'17:15:00','18:00:00'),(16,'18:00:00','18:45:00'),(17,'18:45:00','19:30:00'),(18,'19:30:00','20:15:00'),(19,'20:15:00','21:00:00'),(20,'21:00:00','21:45:00');
/*!40000 ALTER TABLE `periodo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservas`
--

DROP TABLE IF EXISTS `reservas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reservas` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombreAula` varchar(50) DEFAULT NULL,
  `nombreDocente` varchar(100) DEFAULT NULL,
  `horario` int DEFAULT NULL,
  `fechaReserva` date DEFAULT NULL,
  `estado` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `nombreAula` (`nombreAula`),
  KEY `nombreDocente` (`nombreDocente`),
  KEY `horario` (`horario`),
  CONSTRAINT `reservas_ibfk_1` FOREIGN KEY (`nombreAula`) REFERENCES `aula` (`nombre`),
  CONSTRAINT `reservas_ibfk_2` FOREIGN KEY (`nombreDocente`) REFERENCES `usuarios` (`nombre`),
  CONSTRAINT `reservas_ibfk_3` FOREIGN KEY (`horario`) REFERENCES `periodo` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservas`
--

LOCK TABLES `reservas` WRITE;
/*!40000 ALTER TABLE `reservas` DISABLE KEYS */;
INSERT INTO `reservas` VALUES (4,'692F','Miguel',1,'2023-12-06','Aceptado'),(5,'692F','Miguel',2,'2023-12-06','Disponible'),(6,'692F','Miguel',4,'2023-12-06','Disponible'),(7,'692F','Miguel',3,'2023-12-06','Disponible'),(8,'692F','Miguel',5,'2023-12-06','Disponible'),(9,'692F','Miguel',6,'2023-12-06','Disponible'),(11,'691F','Miguel',1,'2023-12-10','Disponible'),(12,'691F','Miguel',1,'2023-12-09','Disponible'),(13,'691F','Miguel',5,'2023-12-09','Disponible'),(14,'691F','Miguel',8,'2023-12-09','Disponible'),(15,'691F','Miguel',14,'2023-12-09','Disponible'),(16,'691F','Miguel',15,'2023-12-09','Disponible'),(17,'691F','Miguel',16,'2023-12-09','Disponible'),(18,'691F','Miguel',17,'2023-12-09','Disponible'),(19,'691F','Miguel',18,'2023-12-09','Disponible'),(20,'691F','Miguel',19,'2023-12-09','Disponible'),(21,'692F','Miguel',1,'2023-12-11','Disponible'),(22,'692F','Miguel',2,'2023-12-11','Disponible'),(23,'692F','Miguel',3,'2023-12-11','Disponible'),(24,'692F','Miguel',4,'2023-12-11','Disponible'),(25,'692F','Miguel',6,'2023-12-11','Disponible'),(26,'692F','Miguel',8,'2023-12-11','Disponible'),(27,'692F','Miguel',9,'2023-12-11','Disponible'),(28,'692F','Miguel',10,'2023-12-11','Disponible'),(29,'692F','Miguel',1,'2023-12-12','Aceptado'),(30,'692F','Miguel',2,'2023-12-12','Pendiente'),(37,'692F','Miguel',1,'2023-12-13','Pendiente'),(38,'691F','Miguel',2,'2023-12-13','Pendiente'),(39,'691F','Miguel',1,'2023-12-13','Pendiente'),(40,'692A','Miguel',1,'2023-12-13','Pendiente'),(41,'650A','Miguel',1,'2023-12-14','Aceptado'),(42,'660A','Miguel',1,'2023-12-13','Pendiente'),(43,'670A','Miguel',1,'2023-12-13','Pendiente'),(44,'650A','Miguel',3,'2023-12-13','Pendiente'),(45,'650A','Miguel',4,'2023-12-13','Pendiente'),(46,'650A','Miguel',1,'2023-12-13','Pendiente'),(47,'650A','Miguel',5,'2023-12-13','Pendiente'),(48,'650A','Miguel',6,'2023-12-13','Aceptado'),(49,'650A','Miguel',17,'2023-12-13','Pendiente'),(50,'650A','Miguel',18,'2023-12-13','Pendiente'),(51,'650B','Miguel',1,'2023-12-13','Pendiente'),(52,'650B','Miguel',3,'2023-12-13','Pendiente'),(53,'650B','Miguel',4,'2023-12-13','Pendiente'),(54,'650B','Miguel',5,'2023-12-13','Pendiente'),(55,'650B','Miguel',6,'2023-12-13','Pendiente'),(56,'650B','Miguel',14,'2023-12-13','Pendiente'),(57,'650B','Miguel',15,'2023-12-13','Pendiente'),(58,'650C','Miguel',7,'2023-12-13','Pendiente'),(59,'650C','Miguel',8,'2023-12-13','Pendiente'),(60,'650C','Miguel',11,'2023-12-13','Pendiente'),(61,'650C','Miguel',12,'2023-12-13','Pendiente'),(62,'650C','Miguel',17,'2023-12-13','Pendiente'),(63,'650C','Miguel',18,'2023-12-13','Pendiente'),(64,'650D','Miguel',1,'2023-12-13','Pendiente'),(65,'650D','Miguel',5,'2023-12-13','Pendiente'),(66,'650D','Miguel',6,'2023-12-13','Pendiente'),(67,'650D','Miguel',13,'2023-12-13','Pendiente'),(68,'650D','Miguel',14,'2023-12-13','Pendiente'),(69,'650D','Miguel',16,'2023-12-13','Pendiente'),(70,'650D','Miguel',17,'2023-12-13','Pendiente'),(71,'650D','Miguel',18,'2023-12-13','Pendiente'),(72,'650A','Miguel',2,'2023-12-13','Pendiente'),(73,'650A','Miguel',8,'2023-12-13','Pendiente'),(75,'692F','Miguel',1,'2023-12-14','Pendiente'),(77,'692F','Miguel',1,'2023-12-16','Pendiente'),(79,'692F','Miguel',18,'2023-12-13','Pendiente'),(83,'680B','Daniel',2,'2023-12-13','Pendiente');
/*!40000 ALTER TABLE `reservas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `usuarios` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(100) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_nombre` (`nombre`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'Mario','mario123','admin'),(2,'Pedro','pedro123','admin'),(3,'Daniel','daniel123','docente'),(4,'Miguel','miguel123','docente');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-12-19 11:03:21
