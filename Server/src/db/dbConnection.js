const mysql = require("mysql");

const connection = mysql.createConnection({
    host: "localhost",
    user: "root",
    password: "19981909",
    database: "reservaumss",
});

function getAllUsers() {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((beginTransactionErr) => {
            if (beginTransactionErr) {
                return reject(beginTransactionErr);
            }
            connection.query('SELECT * FROM usuarios', (err, results) => {
                if (err) {
                    return connection.rollback(() => reject(err));
                }
                connection.commit((commitErr) => {
                    if (commitErr) {
                        return connection.rollback(() => reject(commitErr));
                    }
                    resolve(results);
                });
            });
        });
    });
}

function getAllPeriodos() {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((beginTransactionErr) => {
            if (beginTransactionErr) {
                return reject(beginTransactionErr);
            }
            connection.query('SELECT * FROM periodo', (err, results) => {
                if (err) {
                    return connection.rollback(() => reject(err));
                }
                connection.commit((commitErr) => {
                    if (commitErr) {
                        return connection.rollback(() => reject(commitErr));
                    }
                    resolve(results);
                });
            });
        });
    });
}

function getComprobarAula(nombreAula, inicio, fin, fecha) {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((err) => {
            if (err) {
                reject(err);
                return;
            }
            connection.query(`SELECT * FROM reservas WHERE fechaReserva= '${fecha}' AND nombreAula ='${nombreAula}'`, (err, results) => {
                if (err) {
                    connection.rollback(() => {
                        reject(err);
                    });
                } else {
                    connection.commit((err) => {
                        if (err) {
                            connection.rollback(() => {
                                reject(err);
                            });
                        } else {
                            resolve(results);
                        }
                    });
                }
            });
        });
    });
}

function getAulaConsulta(aula) {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((err) => {
            if (err) {
                reject(err);
                return;
            }
            connection.query(`
                SELECT * FROM aula
                WHERE tipo_aula = '${aula.tipoAula}' AND capacidad >= ${aula.capacidad} AND
                aula.nombre NOT IN (
                    SELECT nombreAula FROM reservas 
                    WHERE fechaReserva = '${aula.fecha.substring(0, 10)}'
                    AND (horario BETWEEN ${aula.horaInicio} AND ${aula.horaFin})
                ) ORDER BY capacidad ASC;;
            `, (err, results) => {
                if (err) {
                    connection.rollback(() => {
                        reject(err);
                    });
                } else {
                    connection.commit((err) => {
                        if (err) {
                            connection.rollback(() => {
                                reject(err);
                            });
                        } else {
                            resolve(results);
                        }
                    });
                }
            });
        });
    });
}

function reserveAmbiente(reservas) {
    return new Promise((resolve, reject) => {
        if (reservas.length === 0) {
            return reject(new Error('El array de reservas está vacío'));
        }
        connection.beginTransaction((err) => {
            if (err) {
                reject(err);
                return;
            }
            const valuesClause = reservas.map(reserva => '(?, ?, ?, ?, ?)').join(', ');
            const query = `INSERT INTO reservas (nombreAula, nombreDocente, horario, estado, fechaReserva) VALUES ${valuesClause};`;
            const values = reservas.flatMap(reserva => [reserva.nombreAula, reserva.usuario, reserva.horaInicio, 'Pendiente', reserva.fecha]);
            connection.query(query, values, (err, results) => {
                if (err) {
                    connection.rollback(() => {
                        reject(err);
                    });
                } else {
                    connection.commit((err) => {
                        if (err) {
                            connection.rollback(() => {
                                reject(err);
                            });
                        } else {
                            resolve(results);
                        }
                    });
                }
            });
        });
    });
}

function getAllReservesDocent(nombreDocente) {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((beginTransactionErr) => {
            if (beginTransactionErr) {
                return reject(beginTransactionErr);
            }
            connection.query(`SELECT * FROM reservas R, periodo P
                WHERE R.horario = P.id AND nombreDocente = '${nombreDocente}'
                ORDER BY fechaReserva DESC FOR UPDATE`, (err, results) => {
                if (err) {
                    return connection.rollback(() => reject(err));
                }
                connection.commit((commitErr) => {
                    if (commitErr) {
                        return connection.rollback(() => reject(commitErr));
                    }
                    resolve(results);
                });
            });
        });
    });
}

function sendUserLogin(username, password) {
    return new Promise((resolve, reject) => {
        const query = "SELECT * FROM usuarios WHERE nombre = ? AND password = ?";
        connection.query(query, [username, password], (err, results) => {
            if (err) {
                reject(err);
            } else {
                resolve(results);
            }
        });
    });
}

function createAula(aula) {
    return new Promise((resolve, reject) => { //falta la parte de tipo de aula
        connection.beginTransaction((beginTransactionErr) => {
            if (beginTransactionErr) {
                return reject(beginTransactionErr);
            }
            connection.query(`INSERT IGNORE INTO aula (nombre, descripcion, capacidad, habilitado, tipo_aula) 
            VALUES ('${aula.nombre}', '${aula.descripcion}', ${aula.capacidad}, ${aula.habilitado}, '${aula.tipo}');`,
                (err, results) => {
                    if (err) {
                        return connection.rollback(() => reject(err));
                    }
                    connection.commit((commitErr) => {
                        if (commitErr) {
                            return connection.rollback(() => reject(commitErr));
                        }
                        resolve(results);
                    });
                });
        });
    });
}

function obtenerTodasReservas() {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((err) => {
            if (err) {
                reject(err);
                return;
            }
            connection.query("SELECT * FROM reservas ORDER BY fechaReserva DESC;", (err, results) => {
                if (err) {
                    connection.rollback(() => {
                        reject(err);
                    });
                } else {
                    connection.commit((err) => {
                        if (err) {
                            connection.rollback(() => {
                                reject(err);
                            });
                        } else {
                            resolve(results);
                        }
                    });
                }
            });
        });
    });
}

function actualizarEstadoAula(id, estado) {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((beginTransactionErr) => {
            if (beginTransactionErr) {
                return reject(beginTransactionErr);
            }
            const sql = "UPDATE reservas SET estado = ? WHERE id = ?";
            connection.query(sql, [estado, id], (error, results) => {
                if (error) {
                    return connection.rollback(() => reject(error));
                }
                connection.commit((commitErr) => {
                    if (commitErr) {
                        return connection.rollback(() => reject(commitErr));
                    }
                    resolve(results);
                });
            });
        });
    });
}

function deshabilitarAula(aula) {
    return new Promise((resolve, reject) => {
        connection.beginTransaction((beginTransactionErr) => {
            if (beginTransactionErr) {
                return reject(beginTransactionErr);
            }
            const sql = `UPDATE aula SET habilitado = false WHERE nombre = ?`;
            connection.query(sql, [aula.aula], (error, results) => {
                if (error) {
                    return connection.rollback(() => reject(error));
                }
                connection.commit((commitErr) => {
                    if (commitErr) {
                        return connection.rollback(() => reject(commitErr));
                    }
                    resolve(results);
                });
            });
        });
    });
}








module.exports = { actualizarEstadoAula, obtenerTodasReservas, getAulaConsulta, getAllReservesDocent, reserveAmbiente, getComprobarAula, deshabilitarAula, createAula, getAllUsers, getAllPeriodos, sendUserLogin }