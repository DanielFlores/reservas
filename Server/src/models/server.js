const express = require("express");
const cors = require("cors");
const jwt = require('jsonwebtoken');
const { actualizarEstadoAula, obtenerTodasReservas, getAulaConsulta, getAllReservesDocent, reserveAmbiente, getComprobarAula, deshabilitarAula, createAula, getAllUsers, getAllPeriodos, sendUserLogin } = require("../db/dbConnection.js");
const e = require("express");
const dotenv = require("dotenv")


class Server {
    constructor() {
        dotenv.config();
        this.app = express();
        this.port = 3003;
        this.app.use(cors());
        this.app.use(express.json()); // para recibir en formato JSON
        this.routes();
    }

    routes() {
        dotenv.config()
        this.app.get('/', (req, res) => {
            res.send("hola mundo desde el servidor");
        });

        this.app.get("/users", validationToken, async(req, res) => {
            try {
                const users = await getAllUsers();
                res.status(200).json(users);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.message });
            }
        });

        this.app.get("/periodos", validationToken, async(req, res) => {
            try {
                const periodos = await getAllPeriodos();
                res.status(200).json(periodos);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.menssage });
            }
        });
        this.app.get("/obtener-Reservas", validationToken, async(req, res) => {
            try {
                const reservados = await obtenerTodasReservas();
                res.status(200).json(reservados);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.message });
            }
        })

        this.app.post("/create-aula", validationToken, async(req, res) => {
            const aula = req.body;
            try {
                const creacion = await createAula(aula);

                const responseObj = {
                    ...creacion,
                    warningCount: creacion.warningCount
                };

                res.status(200).json(responseObj);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.menssage });
            };
        });

        this.app.put("/delete-aula", validationToken, async(req, res) => {
            const aula = req.body;
            try {
                const eliminacion = await deshabilitarAula(aula);
                res.status(200).json(eliminacion);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.message }); // Corrige 'e.menssage' a 'e.message'
            }
        });

        this.app.put("/actualizar-estado", validationToken, async(req, res) => {
            const { id, estado } = req.body;
            try {
                const actualizacion = await actualizarEstadoAula(id, estado);
                res.status(200).json(actualizacion);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.message }); // 
            }
        })

        this.app.post('/reservar-aula', validationToken, async(req, res) => {
            const { nombreAula, horaInicio, horaFin, fecha } = req.body;
            const tokenHeader = req.headers.authorization;
            const token = tokenHeader.split(' ')[1];
            const decodedToken = jwt.verify(token, process.env.SECRET);

            try {
                const reservas = [];

                if (horaInicio !== horaFin) {
                    for (let i = horaInicio; i <= horaFin; i++) {
                        reservas.push({ nombreAula, usuario: decodedToken.usuario, horaInicio: i, fecha });
                    }
                } else {
                    reservas.push({ nombreAula, usuario: decodedToken.usuario, horaInicio, fecha });
                }

                const reserve = await reserveAmbiente(reservas);
                res.status(200).json(reserve);
            } catch (error) {
                console.error('Error en la reserva:', error);
                res.status(500).json({ error: 'Error interno del servidor' });
            }
        });


        this.app.post('/comprobar-aula', validationToken, async(req, res) => {
            const { nombreAula, inicio, fin, fecha } = req.body;
            try {
                const comprobar = await getComprobarAula(nombreAula, inicio, fin, fecha); //comprobar aula
                res.status(200).json(comprobar);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.menssage });
            }
        })
        this.app.post('/reservas-docente', validationToken, async(req, res) => {
            const { nombreDocente } = req.body;
            try {
                const reservasDocente = await getAllReservesDocent(nombreDocente);
                res.status(200).json(reservasDocente);
            } catch (e) {
                console.log(e);
                res.status(500).json({ error: e.menssage });
            }
        })
        this.app.post('/consultar-aula', validationToken, async(req, res) => { //consutlar aula para la reserva
            const aula = req.body;
            try {
                const consultaraula = await getAulaConsulta(aula);
                res.status(200).json(consultaraula);
            } catch (error) {
                console.log(error);
                res.status(500).json({ error: error.menssage });
            }
        })


        this.app.post('/login', async(req, res) => {
            const { usuario, contrasenia } = req.body;

            try {
                const comprobar = await sendUserLogin(usuario, contrasenia);
                if (comprobar.length === 1) {
                    const payload = { usuario };
                    const claveSecreta = process.env.SECRET;
                    const token = jwt.sign(payload, claveSecreta, { expiresIn: '8h' });
                    res.status(200).json({ token, comprobar });
                } else {
                    res.status(401).json({ mensaje: "Credenciales incorrectas" });
                }
            } catch (err) {
                console.error(err);
                if (err.code === 'ER_DUP_ENTRY') {
                    return res.status(409).json({ error: err.message });
                }
                res.status(500).json({ error: err.message });
            }
        });

        function validationToken(req, res, next) {
            const accessToken = req.headers['authorization'];
            const acceso = accessToken.split(" ")[1]
            if (!accessToken) {
                return res.status(401).json({ error: 'Acceso denegado, token no proporcionado' });
            }
            const clave = process.env.SECRET;
            jwt.verify(acceso, clave, (err, user) => {
                if (err) {
                    console.error('Error al verificar el token:', err);
                    if (err.name === 'TokenExpiredError') {
                        return res.status(401).json({ error: 'Acceso denegado, token expirado' });
                    } else {
                        return res.status(401).json({ error: 'Acceso denegado, token incorrecto' });
                    }
                } else {
                    console.log('Token verificado correctamente:', user);
                    req.user = user;
                    next();
                }
            });
        }


    }
    start() {
        this.app.listen(this.port, () => {
            console.log(`se conceto con exito en el puerto: ${this.port}`);
        });
    }
}

module.exports = Server;