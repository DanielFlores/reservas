# Nombre del Proyecto : ReservaUmss

Es una app de una paguina web para reservas de aulas de la universidad.
Tanto los administrativos como los docentes no se puede registrar ya que el encargado de la base de datos le,
debe de proporcionar un nombre de usuario y una contraseña

## Requisitos Previos

El proyecto esta dividido en dos partes:

Una parte del Cliente que esta en la carpeta "Client" consta de un proyecto con node.js,
con el frimework de React creado con Vite.

La otra parte es el Servidor que esta en la carpeta de "Server" que esta funciona con Node.js, Express y Cors.

Con una base de datos en MySql.

Asegúrate de tener instalados los siguientes programas antes de ejecutar el proyecto:

- Node.js
- npm

## Configuración

1. Clona el repositorio:

   ```bash
   git clone https://gitlab.com/DanielFlores/reservas.git

   ```

2. Entra al proyecto
   ```bash
   cd reservas
   ```
3. Entra al servidor

   ```bash
   cd Server
   npm install
   mysql -u usuario -pcontraseña < baseDeDatos.sql
   "en mi caso yo use la contraseña de : '19981909'"
   npm run dev
   cd ..

   ```

4. Entra al cliente

   ```bash
   cd Client
   npm install
   npm run dev

   ```

5. por defecto el proyecto se lanzara en el direccion url de ttp://localhost:5173/

6. los datos de Admin por defecto son :

   - nombre de usuario : Miguel
   - password : miguel123

   los datos del docente son:

   - nombre usuario : Mario
     -password : mario123
