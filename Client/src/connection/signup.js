export const loginUser = async(usuario, contrasenia) => {
    try {
        const curretUser = await fetch('http://localhost:3003/login', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ usuario, contrasenia })
        });
        if (curretUser.ok) {
            const data = await curretUser.json();
            const token = data ? data.token : null;
            const usuario = data ? data.comprobar[0].nombre : null;

            if (token) {
                localStorage.setItem('token', token);
                localStorage.setItem('user', usuario);
                return data;
            } else {
                console.log('La respuesta no contiene un token.');
            }
        } else {
            console.log('Credenciales incorrectas');
        }
    } catch (error) {
        console.log(error.message);
    }
};