const getToken = () => localStorage.getItem('token');

export const actualizarEstado = async(id, estado) => {
    const token = getToken();
    try {
        const currertuser = await fetch('http://localhost:3003/actualizar-estado', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ id: id, estado: estado })
        });
        if (currertuser.ok) {
            const data = await currertuser.json();
            return data
        } else {
            console.log('no modico el estado del aula');
        }
    } catch (error) {
        console.log("ocurrio un problema al actualizar el estado ")
        console.error(error.message)
    }
}
export const obtenerReservados = async() => {
    const token = getToken();
    try {
        const reservas = await fetch('http://localhost:3003/obtener-Reservas', {
            method: 'GET',
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });
        if (reservas.ok) {
            const reservados = await reservas.json();
            return reservados;
        }
    } catch (error) {
        console.log("ocurrio un problema al obtener las reservas ")
        console.error(error)
    }
}

export const consultarAula = async(aulaConsulta) => {
    const token = getToken();
    try {
        const consulta = await fetch('http://localhost:3003/consultar-aula', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(aulaConsulta)

        })
        if (consulta.ok) {
            const data = await consulta.json();
            return data
        } else {
            console.log('no se consulto nada');
        }
    } catch (error) {
        console.log("ocurrio un problema al consultar un aula en el crud")
        console.error(error)
    }
}

export const obtenerReservasDocente = async(nombreDocente) => {
    const token = getToken();
    try {
        const reservesDocente = await fetch('http://localhost:3003/reservas-docente', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ nombreDocente }) // Envolvemos el nombreDocente en un objeto
        });
        if (!reservesDocente.ok) {
            throw new Error("La solicitud no fue exitosa");
        }

        const periodosData = await reservesDocente.json();
        return periodosData;
    } catch (error) {
        console.error("no se pudo obtener las reservas docente", error);
        throw error;
    }
}
export const reservarAula = async(aula) => {
    const token = getToken();

    try {
        const currentUser = await fetch('http://localhost:3003/comprobar-aula', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(aula)
        });
        if (currentUser.ok) {
            const data = await currentUser.json();
            const hayReservado = verificarAu(data, aula);

            if (hayReservado.length === 0) {
                console.log("Se puede reservar porque hay espacio");
                const reservar = await fetch('http://localhost:3003/reservar-aula', {
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json',
                        'Authorization': `Bearer ${token}`
                    },
                    body: JSON.stringify(aula)
                });
                if (reservar.ok) {
                    const datoRes = await reservar.json();
                    return datoRes
                }
            } else {
                return "NoReserva";
            }
        }
    } catch (error) {
        console.log(error.message);
        return "Error al procesar la reserva";
    }
}

const verificarAu = (base, aula) => {
    const hora1 = aula.horaInicio;
    const hora2 = aula.horaFin;
    const array = [];

    if (hora1 !== hora2) {
        const arregloNumeros = Array.from({ length: +hora2 - +hora1 + 1 }, (_, index) => +hora1 + index);

        for (let i = 0; i < base.length; i++) {
            for (let j = 0; j < arregloNumeros.length; j++) {
                if (base[i].horario === arregloNumeros[j]) {
                    array.push(base[i]);
                }
            }
        }
    } else {
        const numero = +hora1;
        const reservasConHorarioEspecifico = base.filter(reserva => reserva.horario === numero);
        array.push(...reservasConHorarioEspecifico);
    }
    return array;
}


export const eliminarAula = async(aula) => {
    const token = getToken();
    try {
        const currertuser = await fetch('http://localhost:3003/delete-aula', {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify({ aula })
        });
        if (currertuser.ok) {
            const data = await currertuser.json();
            return data
        } else {
            console.log('no se elimino el aula');
        }
    } catch (error) {
        console.log(error.message);
    }
};

export const verificarAula = async(aula) => {
    const token = getToken();
    try {
        const curretUser = await fetch('http://localhost:3003/create-aula', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            },
            body: JSON.stringify(aula)
        });
        if (curretUser.ok) {
            const data = await curretUser.json();
            if (data.warningCount === 1) {
                return 1
            } else {
                return 0
            }

        } else {
            // Maneja la respuesta si las credenciales son incorrectas, etc.
            console.log('no se creo el aula');
        }
    } catch (error) {
        console.log(error.message);
    }
};
export const periodosHorario = async() => {
    const token = getToken();
    try {
        const response = await fetch("http://localhost:3003/periodos", {
            headers: {
                Authorization: `Bearer ${token}`,
            },
        });

        if (!response.ok) {
            throw new Error("La solicitud no fue exitosa");
        }
        const periodosData = await response.json();
        return periodosData;
    } catch (error) {
        console.error("Error al obtener datos:", error);
    }
}