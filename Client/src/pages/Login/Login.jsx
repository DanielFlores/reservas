import { useState, useEffect } from "react";
import logo from "../../Imagenes/logo_inicio_sesion.jpg";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faEyeSlash,
  faEye,
  faCircleExclamation,
} from "@fortawesome/free-solid-svg-icons";
import "./Login.css";
import { useFormik } from "formik";
import * as Yup from "yup";

import { loginUser } from "../../connection/signup";

const Login = () => {
  const [showPassword, setShowPassword] = useState(false);
  const [showError, setShowError] = useState(false);

  const [errorVeri, setErrorVeri] = useState("");
  const [clearPassword, setClearPassword] = useState(false);

  const navigate = useNavigate();

  const schema = Yup.object().shape({
    username: Yup.string().min(1).required(),
    password: Yup.string().min(1).required(),
  });

  const { handleSubmit, handleChange, errors, values } = useFormik({
    initialValues: {
      username: "",
      password: "",
    },

    onSubmit: (values) => {
      event.preventDefault();
      handleLogin(values);
    },
    validationSchema: schema,
  });

  const handleLogin = async (values) => {
    try {
      const currentUser = await loginUser(values.username, values.password);

      if (currentUser) {
        console.log("Inicio de sesión exitoso:", currentUser);

        localStorage.setItem("accessToken", currentUser.token);

        if (currentUser.comprobar[0].role === "docente") {
          navigate("/docente", { replace: true });
        } else if (currentUser.comprobar[0].role === "admin") {
          navigate("/administrador", { replace: true }); // no funciona no esta bien
        }
      } else {
        console.log("No se pudo obtener información del usuario.");
        console.log(showError);
        setErrorVeri("Nombre de usuario o contraseña incorrectos.");
        setShowError(true);
        setClearPassword(true);
      }
    } catch (error) {
      console.error("Error al iniciar sesión:", error);
      setErrorVeri("Nombre de usuario o contraseña incorrectos.");
      setShowError(true);
      setClearPassword(true);
    }
  };
  useEffect(() => {
    if (clearPassword) {
      handleChange({ target: { name: "password", value: "" } });
      setClearPassword(false);
    }
  }, [clearPassword, handleChange]);

  return (
    <div className="rounded-lg w-full min-h-screen backdrop-blur-md bg-black/30 flex items-center justify-center ">
      <div className="p-12  shadow-xl rounded-xl relative z-1 overflow-hidden bg-white ">
        <form onSubmit={handleSubmit} className="space-y-4">
          <div className="flex justify-center">
            <img className=" w-32 flex justify-center" src={logo} alt="logo" />
          </div>
          <p className="flex justify-center font-bold text-xl leading-tight tracking-tight text-gray-900 dark:text-black">
            Iniciar Sesión
          </p>
          <div className="py-2">
            {showError && (
              <div className="pl-3 py-3 -mx-10 bg-red-600 ">
                <FontAwesomeIcon
                  icon={faCircleExclamation}
                  style={{ fontSize: "2rem", color: "#ffffff" }}
                />
                <span className="pl-3 text-white">{errorVeri}</span>
              </div>
            )}
          </div>
          <div className="relative">
            <input
              type="text"
              name="username"
              id="username"
              className="w-full p-3 border bg-white border-emerald-500 rounded outline:border-emerald-500 focus:outline-none text-black"
              placeholder="Nombre de usuario"
              onChange={handleChange}
            />
          </div>
          {errors.username && (
            <span className="text-sm text-red-500">
              Ingrese el nombre de usuario
            </span>
          )}
          <div className="relative">
            <input
              type={showPassword ? "text" : "password"}
              name="password"
              id="password"
              placeholder="Contraseña"
              value={values.password}
              className="w-full p-3 border bg-white border-emerald-500 rounded outline:border-emerald-500 focus:outline-none text-black "
              onChange={handleChange}
            />
            <div className="absolute inset-y-0 right-0 pr-3 flex items-center cursor-pointer">
              <FontAwesomeIcon
                icon={showPassword ? faEye : faEyeSlash}
                onClick={() => setShowPassword(!showPassword)}
                className="text-gray-400"
              />
            </div>
          </div>
          {errors.password && (
            <span className="text-sm text-red-500">Ingrese la contraseña</span>
          )}
          <button
            type="submit"
            className="w-full h-10 bg-emerald-500 rounded-full text-white hover:bg-emerald-700 flex items-center justify-center"
          >
            Iniciar Sesión
          </button>
        </form>
      </div>
    </div>
  );
};

export default Login;
