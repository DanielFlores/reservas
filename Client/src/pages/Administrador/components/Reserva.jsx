import PropTypes from "prop-types";
import { useState } from "react";
import { actualizarEstado } from "../../../connection/crud";
const Reserva = ({
  id,
  nombreAula,
  nombreDocente,
  horario,
  fechaReserva,
  estado,
}) => {
  const fechaCortada = fechaReserva.substring(0, 10);
  const [est, setEst] = useState(estado);
  const [identificador] = useState(id);
  const getColorClass = (estadoo) => {
    switch (estadoo) {
      case "Pendiente":
        return "bg-yellow-300";
      case "Aceptado":
        return "bg-green-500";
      case "Rechazado":
        return "bg-red-500";
      default:
        return "bg-gray-500";
    }
  };
  const handleEstadoChange = (e) => {
    const nuevoEstado = e.target.value;
    setEst(nuevoEstado);
    cambiarEstado(nuevoEstado);
  };
  const cambiarEstado = async (nuevoEstado) => {
    const respuesta = await actualizarEstado(identificador, nuevoEstado);
    console.log(respuesta);
  };
  return (
    <div
      className={`grid grid-flow-col-1 gap-2 ${getColorClass(
        est
      )} p-3 rounded-xl`}
    >
      <h1 className="text-sm">{id}</h1>
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Nombre Aula:</h1>
        <h1 className="font-mono">{nombreAula}</h1>
      </div>
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Docente:</h1>
        <h1 className="font-mono">{nombreDocente}</h1>
      </div>
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Horario</h1>
        <h1 className="font-mono">{horario}</h1>
      </div>

      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Fecha Reserva</h1>
        <h1 className="font-mono"> {fechaCortada}</h1>
      </div>
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Estado</h1>
        <select className="font-mono" value={est} onChange={handleEstadoChange}>
          <option value="Rechazado">Rechazado</option>
          <option value="Aceptado">Aceptado</option>
          <option value="Pendiente">Pendiente</option>
        </select>
      </div>
    </div>
  );
};
Reserva.propTypes = {
  id: PropTypes.number.isRequired,
  nombreAula: PropTypes.string.isRequired,
  nombreDocente: PropTypes.string.isRequired,
  horario: PropTypes.number.isRequired,
  fechaReserva: PropTypes.string.isRequired,
  estado: PropTypes.string.isRequired,
};
export default Reserva;
