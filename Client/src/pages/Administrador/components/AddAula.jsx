import { useFormik } from "formik";
import * as Yup from "yup";
import { verificarAula } from "../../../connection/crud";
import Swal from "sweetalert2";
import ErrorMessage from "./ErrorMessage";

const AddAula = () => {
  const initialValues = {
    nombre: "",
    tipo: "",
    descripcion: "",
    capacidad: 0,
    habilitado: true,
  };

  const tipoOptions = ["laboratorio", "aula-comun", "auditorio"];

  const validationSchema = Yup.object().shape({
    nombre: Yup.string()
      .required("El nombre de aula es obligatorio")
      .trim("No se permiten espacios al inicio o al final")
      .strict(true)
      .min(2, "El nombre debe tener mínimo 2 caracteres")
      .max(30, "El nombre debe tener máximo 30 caracteres"),
    descripcion: Yup.string()
      .trim("No se permiten espacios al inicio o al final")
      .max(255, "La descripción debe tener máximo 255 caracteres"),
    capacidad: Yup.number()
      .required("La capacidad del aula es obligatoria")
      .positive("La capacidad debe ser un número positivo")
      .integer("La capacidad debe ser un número entero")
      .max(500, "La capacidad máxima debe ser de 500"),
    tipo: Yup.string()
      .required("Seleccione el tipo de aula")
      .oneOf(tipoOptions, "Tipo de aula no válido"),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      console.log(values);
      addNewAula({
        nombre: formik.values.nombre,
        descripcion: formik.values.descripcion,
        tipo: formik.values.tipo,
        capacidad: formik.values.capacidad,
        habilitado: true,
      });
    },
  });

  const addNewAula = async (aula) => {
    try {
      const resultado = await verificarAula(aula);

      if (resultado === 0) {
        Swal.fire({
          title: "Éxito!",
          text: "Se ha creado correctamente el aula",
          icon: "success",
          confirmButtonText: "Aceptar",
          confirmButtonColor: "#10b981",
        }).then((result) => {
          if (result.isConfirmed) {
            formik.resetForm();
            console.log("Se agregó correctamente");
          }
        });
      } else {
        Swal.fire({
          title: "Aula Existente",
          text: "Ya existe un aula con ese nombre",
          icon: "error",
          confirmButtonText: "Aceptar",
        }).then((result) => {
          if (result.isConfirmed) {
            console.log("no se agrego aula si duplicado");
            formik.resetForm();
          }
        });
      }
    } catch (error) {
      Swal.fire({
        title: "Error!",
        text: "Ha ocurrido un error al crear el aula",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
  };

  return (
    <div>
      <div className="flex justify-center font-bold text-2xl">
        <h1>Deshabilitar Aula</h1>
      </div>
      <form
        onSubmit={formik.handleSubmit}
        className="w-full h-full bg-white flex flex-col items-center justify-between gap-4 p-10 sm:w-full"
      >
        <div className="w-full">
          <p>Nombre Aula:</p>
          <input
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
            type="text"
            placeholder="Nombre"
            name="nombre"
            onChange={formik.handleChange}
            value={formik.values.nombre}
          />
          <ErrorMessage message={formik.errors.nombre} />
        </div>
        <div className="w-full">
          <p>Breve descripcion</p>
          <input
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
            type="text"
            placeholder="Descripción"
            name="descripcion"
            onChange={formik.handleChange}
            value={formik.values.descripcion}
          />
          <ErrorMessage message={formik.errors.descripcion} />
        </div>
        <div className="w-full">
          <p>Tipo de Aula</p>
          <select
            className="p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none "
            name="tipo"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.tipo}
          >
            <option
              value=""
              label="Seleccione el tipo de aula"
              disabled
              className=" p-4 h-4"
            />
            {tipoOptions.map((option) => (
              <option
                key={option}
                value={option}
                className="bg-white p-12 mb-2"
              >
                {option}
              </option>
            ))}
          </select>
          <ErrorMessage message={formik.errors.tipo} />
        </div>
        <div className="w-full">
          <p>Capacidad de aula</p>
          <input
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
            type="number"
            placeholder="Capacidad"
            name="capacidad"
            onChange={formik.handleChange}
            value={formik.values.capacidad}
          />
          <ErrorMessage message={formik.errors.capacidad} />
        </div>
        <button
          type="submit"
          className="w-full h-10 bg-emerald-500 rounded-full text-white hover:bg-emerald-700"
        >
          Crear aula
        </button>
      </form>
    </div>
  );
};

export default AddAula;
