import PropTypes from "prop-types";
const ErrorMessage = ({ message }) => {
  return (
    <>
      {message && (
        <div className="flex justify-start text-red-500 text-xs">{message}</div>
      )}
    </>
  );
};

ErrorMessage.propTypes = {
  message: PropTypes.string,
};
export default ErrorMessage;
