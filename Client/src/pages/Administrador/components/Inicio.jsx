const Inicio = () => {
  return (
    <div className="bg-gray-300 h-52">
      <h1 className="flex justify-center text-3xl font-bold p-3">
        Estoy en Inicio
      </h1>
      <h1 className="flex justify-center font-semibold py-5">
        Bienvenido al panel de inicio de administrador.
      </h1>
    </div>
  );
};

export default Inicio;
