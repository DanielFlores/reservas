import * as Yup from "yup";
import { useFormik } from "formik";
import ErrorMessage from "./ErrorMessage";
import { eliminarAula } from "../../../connection/crud";
import Swal from "sweetalert2";
const EliminarAula = () => {
  const initialValues = {
    aula: "",
  };
  const validationSchema = Yup.object().shape({
    aula: Yup.string()
      .required("El nombre de aula es obligatorio")
      .trim("No se permiten espacios al inicio o al final")
      .strict(true)
      .min(2, "El nombre debe tener mínimo 2 caracteres")
      .max(30, "El nombre debe tener máximo 30 caracteres"),
  });
  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      deleteAula(values.aula);
    },
  });
  const deleteAula = async (aula) => {
    try {
      const resultado = await eliminarAula(aula);
      if (resultado.changedRows === 1) {
        Swal.fire({
          title: "Éxito!",
          text: "Se deshabilitado el aula",
          icon: "success",
          confirmButtonText: "Aceptar",
          confirmButtonColor: "#10b981",
        }).then((result) => {
          if (result.isConfirmed) {
            formik.resetForm();
            console.log("Se deshabilito correctamente");
          }
        });
      } else {
        Swal.fire({
          title: "El aula esta deshabilitado",
          text: "El aula ya esta deshabilitado",
          icon: "error",
          confirmButtonText: "Aceptar",
        }).then((result) => {
          if (result.isConfirmed) {
            console.log("ya esta deshabilitado");
            formik.resetForm();
          }
        });
      }
    } catch (error) {
      Swal.fire({
        title: "Error!",
        text: "Ha ocurrido un error al crear el aula",
        icon: "error",
        confirmButtonText: "Aceptar",
      });
    }
  };
  return (
    <div className="">
      <div className="flex justify-center font-bold text-2xl">
        <h1>Deshabilitar aulas</h1>
      </div>
      <form
        onSubmit={formik.handleSubmit}
        className="w-full h-full bg-white flex flex-col items-center justify-between gap-4 p-10 sm:w-full"
      >
        <div className="w-full">
          <p>Nombre Aula:</p>
          <input
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
            type="text"
            placeholder="Aula"
            name="aula"
            onChange={formik.handleChange}
            value={formik.values.aula}
          />
          <ErrorMessage message={formik.errors.aula} />
        </div>
        <button
          type="submit"
          className="w-full h-10 bg-emerald-500 rounded-full text-white hover:bg-emerald-700"
        >
          Deshabilitar aula
        </button>
      </form>
    </div>
  );
};

export default EliminarAula;
