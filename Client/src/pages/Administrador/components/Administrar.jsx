import { useState, useEffect } from "react";
import { obtenerReservados } from "../../../connection/crud";
import Reserva from "./Reserva";

const Administrar = () => {
  const [reservas, setReservas] = useState([]);

  useEffect(() => {
    const fetchData = async () => {
      const reser = await obtenerReservados();
      setReservas(reser);
    };

    fetchData();
  }, []);
  return (
    <div className="bg-gray-300 p-4 ">
      <div className="flex justify-center font-bold text-2xl">
        <h1>Crear Aula</h1>
      </div>
      <div className="w-full flex flex-col gap-4">
        {" "}
        {reservas.map((reservado, index) => (
          <Reserva
            key={index}
            id={reservado.id}
            nombreAula={reservado.nombreAula}
            nombreDocente={reservado.nombreDocente}
            horario={reservado.horario}
            fechaReserva={reservado.fechaReserva}
            estado={reservado.estado}
          />
        ))}
      </div>
    </div>
  );
};
export default Administrar;
