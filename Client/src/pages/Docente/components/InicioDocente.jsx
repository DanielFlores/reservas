import { useState, useEffect } from "react";
import { obtenerReservasDocente } from "../../../connection/crud";
import Reservado from "./Reservado";
const InicioDocente = () => {
  const [reservados, setReservados] = useState([]);
  const usuario = localStorage.getItem("user");
  useEffect(() => {
    const fetchData = async () => {
      try {
        const reservasData = await obtenerReservasDocente(usuario);
        setReservados(reservasData);
      } catch (error) {
        console.error("Error al obtener reservas del docente:", error.message);
      }
    };

    fetchData();
  }, []);
  return (
    <div
      className="container mx-auto bg-slate-100"
      style={{ minHeight: "400px" }}
    >
      <div className="text-xl font-bold flex flex-col items-center pb-4">
        <h1>Inicio Docente</h1>
      </div>
      <div className="w-full flex flex-col gap-4">
        {reservados.map((reserva, index) => (
          <Reservado
            key={index}
            nombreAula={reserva.nombreAula}
            horaInicio={reserva.h_inicio}
            horaFin={reserva.h_final}
            fechaReserva={reserva.fechaReserva}
            estado={reserva.estado}
          />
        ))}
      </div>
    </div>
  );
};

export default InicioDocente;
