import { useState, useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import ErrorMessage from "../../Administrador/components/ErrorMessage";
import Swal from "sweetalert2";
import { reservarAula, periodosHorario } from "../../../connection/crud";
import "sweetalert2/src/sweetalert2.scss";
const Reserva = () => {
  const [periodos, setPeriodos] = useState([]);
  const initialValues = {
    nombreAula: "",
    horaInicio: 1,
    horaFin: 1,
    fecha: new Date(),
  };
  const validationSchema = Yup.object().shape({
    nombreAula: Yup.string()
      .required("El nombre de aula es obligatorio")
      .trim("No se permiten espacios al inicio o al final")
      .strict(true)
      .min(2, "El nombre debe tener mínimo 2 caracteres")
      .max(30, "El nombre debe tener máximo 30 caracteres"),
    horaInicio: Yup.number().required(
      "La selección del horario de inicio es obligatoria"
    ),
    horaFin: Yup.number()
      .required("La selección del horario de fin es obligatoria")
      .test(
        "horaFinSuperior",
        "La hora de fin debe ser mayor que la hora de inicio",
        function (value) {
          const horaInicio = this.resolve(Yup.ref("horaInicio"));
          return value >= horaInicio;
        }
      ),
  });

  const formik = useFormik({
    initialValues,
    validationSchema,
    validateOnChange: true,
    onSubmit: (values) => {
      reservar(values);
    },
  });
  const reservar = async (aula) => {
    try {
      const fechaFormateada = aula.fecha.toISOString().split("T")[0];
      const aulaFormateada = { ...aula, fecha: fechaFormateada };
      const reser = await reservarAula(aulaFormateada);

      if (reser) {
        if (reser === "NoReserva") {
          Swal.fire({
            title: "¡Reserva No Realizada!",
            text: "El aula ya esta reservado.",
            icon: "error",
            confirmButtonText: "OK",
            customClass: {
              popup: "vertical-popup",
            },
          });
        } else {
          Swal.fire({
            title: "¡Reserva Exitosa!",
            text: "El aula se ha reservado correctamente.",
            icon: "success",
            confirmButtonText: "OK",
            customClass: {
              popup: "vertical-popup",
            },
          }).then((result) => {
            if (result.isConfirmed) {
              formik.resetForm();
              console.log("Se reservo correctamente");
            }
          });
        }
      }
    } catch (error) {
      console.error(error);
      Swal.fire({
        title: "Error!",
        text: "Ha ocurrido un error al crear el aula",
        icon: "error",
        confirmButtonText: "Aceptar",
        customClass: {
          popup: "vertical-popup",
        },
      });
    }
  };
  useEffect(() => {
    const fetchData = async () => {
      const periodosData = await periodosHorario();
      setPeriodos(periodosData);
    };

    fetchData();
  }, []);

  return (
    <div className="container mx-auto">
      <div className="text-xl font-bold flex flex-col items-center">
        <h1>reserva aula</h1>
      </div>
      <form
        onSubmit={formik.handleSubmit}
        className="w-full h-full bg-gray-100 rounded-md flex flex-col items-center justify-between gap-4 p-10 sm:w-full"
      >
        <div className="w-full">
          <p>Nombre Aula:</p>
          <input
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
            type="text"
            placeholder="Nombre"
            name="nombreAula"
            onChange={formik.handleChange}
            value={formik.values.nombreAula}
          />
          <ErrorMessage message={formik.errors.nombreAula} />
        </div>
        <div className="w-full">
          <p>Hora de inicio:</p>
          <select
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
            name="horaInicio"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.horaInicio}
          >
            <option value="" label="Seleccione la hora" disabled />

            {periodos.map((periodo) => (
              <option key={periodo.id} value={periodo.id}>
                {`${periodo.h_inicio}`}
              </option>
            ))}
          </select>
          <ErrorMessage message={formik.errors.horaInicio} />
        </div>
        <div>
          <label htmlFor="horaFin" className="text-sm">
            Hora de finalización:
          </label>
          <select
            id="horaFin"
            name="horaFin"
            className=" p-3 border border-emerald-500 rounded "
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.horaFin}
          >
            <option value="" disabled className="w-full bg-black h-1">
              Seleccione la hora
            </option>
            {periodos.map((periodo) => (
              <option key={periodo.id} value={periodo.id} className="w-full">
                {`${periodo.h_final}`}
              </option>
            ))}
          </select>
          <ErrorMessage message={formik.errors.horaFin} />
        </div>
        <div className="w-full">
          <p>Fecha</p>
          <DatePicker
            selected={formik.values.fecha}
            onChange={(date) => formik.setFieldValue("fecha", date)}
            dateFormat="dd/MM/yyyy"
            className="w-full p-3 border border-emerald-500 rounded outline:border-emerald-500 focus:outline-none"
          />
          <ErrorMessage message={formik.errors.fecha} />
        </div>

        <button
          type="submit"
          className="w-full h-10 bg-emerald-500 rounded-full text-white hover:bg-emerald-700"
        >
          Reservar aula
        </button>
      </form>
    </div>
  );
};

export default Reserva;
