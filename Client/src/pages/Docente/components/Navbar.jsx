import PropTypes from "prop-types";
import { useNavigate } from "react-router-dom";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBars,
  faQuestion,
  faRightFromBracket,
  faHouse,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";
import { useState } from "react";
import { Link } from "react-router-dom";
import logo from "../../../Imagenes/logo.jpeg";

const Navbar = ({ token }) => {
  const [menuVisible, setMenuVisible] = useState(false);
  const user = localStorage.getItem("user");
  const navigate = useNavigate();
  const barra = () => {
    if (menuVisible === true) {
      setMenuVisible(false);
    } else {
      setMenuVisible(true);
    }
  };
  const hideMenu = () => {
    setMenuVisible(false);
  };

  const showMenu = () => {
    setMenuVisible(true);
  };

  const handleLogout = () => {
    localStorage.removeItem("token");
    localStorage.removeItem("accessToken");
    navigate("/");
    console.log(token);
  };

  return (
    <div className="bg-gray-600 text-white p-4">
      <div className="container mx-auto flex justify-between items-center">
        <div className="text-2xl font-bold pl-5">
          <img className="w-12" src={logo} alt="logo" />
        </div>
        <div className="text-xl font-bold">
          <h1>Docente: {user}</h1>
        </div>
        <div
          onMouseEnter={showMenu}
          onMouseLeave={hideMenu}
          className="relative inline-block text-left pr-5"
        >
          <button
            className="text-gray-500 hover:text-emerald-500 focus:outline-none "
            onClick={barra}
          >
            <FontAwesomeIcon icon={faBars} size="lg" />
          </button>
          {menuVisible && (
            <div
              onMouseEnter={showMenu}
              onMouseLeave={hideMenu}
              className="origin-top-right absolute right-1 mt-1 w-60  rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 z-20"
            >
              <div
                className="py-1 "
                role="menu"
                aria-orientation="vertical"
                aria-labelledby="options-menu"
              >
                <Link to="">
                  <button
                    onClick={hideMenu}
                    className="block w-full px-4 py-2 text-lg text-gray-700 hover:bg-emerald-100 flex items-center"
                    role="menuitem"
                  >
                    <span className="mr-2">
                      <FontAwesomeIcon icon={faHouse} />
                    </span>
                    Inicio
                  </button>
                </Link>
                <Link to="consultar">
                  <button
                    onClick={hideMenu}
                    className="block w-full px-4 py-2 text-lg text-gray-700 hover:bg-emerald-100 flex items-center"
                    role="menuitem"
                  >
                    <span className="mr-2">
                      <FontAwesomeIcon icon={faQuestion} />
                    </span>
                    Consultar aula
                  </button>
                </Link>
                <Link to="reserva">
                  <button
                    onClick={hideMenu}
                    className="block w-full px-4 py-2 text-lg text-gray-700 hover:bg-emerald-100 flex items-center"
                    role="menuitem"
                  >
                    <span className="mr-2">
                      <FontAwesomeIcon icon={faPlus} />
                    </span>
                    Reservar aula
                  </button>
                </Link>

                <button
                  onClick={handleLogout}
                  className="block w-full px-4 py-2 text-lg text-gray-700 hover:bg-emerald-100 flex items-center"
                  role="menuitem"
                >
                  <span className="mr-2">
                    <FontAwesomeIcon icon={faRightFromBracket} />
                  </span>
                  Cerrar Sesión
                </button>
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

Navbar.propTypes = {
  token: PropTypes.string.isRequired,
};

export default Navbar;
