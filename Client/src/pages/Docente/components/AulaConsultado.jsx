import PropTypes from "prop-types";

const AulaConsultado = ({ nombre, descripcion, capacidad, tipoaula }) => {
  return (
    <div className="grid grid-flow-col-1 gap-2 bg-cyan-200 p-3 rounded-xl">
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Nombre Aula:</h1>
        <h1 className="font-mono">{nombre}</h1>
      </div>
      <div className="grid grid-flow-col-1 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Descripcion Aula:</h1>
        <h2 className="font-mono">{descripcion}</h2>
      </div>
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">Capacidad:</h1>
        <h1 className="font-mono">{capacidad}</h1>
      </div>
      <div className="grid grid-cols-2 gap-4 bg-red-100 px-3">
        <h1 className="text-lg font-roboto font-semibold">tipo:</h1>
        <h1 className="font-mono">{tipoaula}</h1>
      </div>
    </div>
  );
};

AulaConsultado.propTypes = {
  nombre: PropTypes.string.isRequired,
  descripcion: PropTypes.string.isRequired,
  capacidad: PropTypes.number.isRequired,
  tipoaula: PropTypes.string.isRequired,
};

export default AulaConsultado;
