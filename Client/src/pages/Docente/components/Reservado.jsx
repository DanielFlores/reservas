import PropTypes from "prop-types";

const Reservado = ({
  nombreAula,
  horaInicio,
  horaFin,
  fechaReserva,
  estado,
}) => {
  const fechaCortada = fechaReserva.substring(0, 10);
  const getColorClass = () => {
    switch (estado) {
      case "Pendiente":
        return "bg-yellow-300";
      case "Reservado":
        return "bg-green-500";
      case "Rechazado":
        return "bg-red-500";
      default:
        return "bg-gray-500";
    }
  };
  return (
    <div className={`${getColorClass()} rounded-lg`}>
      <div className=" p-4 grid grid-cols-7 gap-2 font-Roboto ">
        <div className=" bg-gray-200 col-span-3 text-2xl">
          Aula: {nombreAula}
        </div>
        <div className="bg-gray-200 col-span-4 text-xl">
          Fecha: {fechaCortada}
        </div>
        <div className="bg-gray-200 col-span-7 text-xl">
          Horario: {horaInicio} - {horaFin}
        </div>
        <div className="bg-blue-200 col-span-7 text-xl flex justify-center font-bold">
          {estado}
        </div>
      </div>
    </div>
  );
};
Reservado.propTypes = {
  nombreAula: PropTypes.string.isRequired,
  horaInicio: PropTypes.string.isRequired,
  horaFin: PropTypes.string.isRequired,
  fechaReserva: PropTypes.string.isRequired,
  estado: PropTypes.string.isRequired,
};

export default Reservado;
