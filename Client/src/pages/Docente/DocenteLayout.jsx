import { Outlet } from "react-router-dom";
import Navbar from "./components/Navbar";
const DocenteLayout = () => {
  const token = localStorage.getItem("token");

  return (
    <div className="flex flex-col h-full w-full bg-white overflow-hidden ">
      {/* Barra de navegación */}
      <Navbar token={token} />
      {/* Contenido principal */}
      <div className="overflow-y-auto bg-white p-5 grow">
        <Outlet />
      </div>
    </div>
  );
};
export default DocenteLayout;
