import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./pages/Login/Login";
import AdminLayout from "./pages/Administrador/AdminLayout";
import DocenteLayout from "./pages/Docente/DocenteLayout";
import Inicio from "./pages/Administrador/components/Inicio";
import Administrar from "./pages/Administrador/components/Administrar";
import AddAula from "./pages/Administrador/components/AddAula";
import DeleteAula from "./pages/Administrador/components/DeleteAula";
import Reserva from "./pages/Docente/components/Reserva";
import InicioDocente from "./pages/Docente/components/InicioDocente";
import Consultar from "./pages/Docente/components/Consultar";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Login />} />
        <Route path="/docente" element={<DocenteLayout />}>
          <Route path="" index element={<InicioDocente />} />
          <Route path="reserva" element={<Reserva />} />
          <Route path="consultar" element={<Consultar />} />
        </Route>
        <Route path="/administrador" element={<AdminLayout />}>
          <Route path="" index element={<Inicio />} />
          <Route path="admin" element={<Administrar />} />
          <Route path="add-aula" element={<AddAula />} />
          <Route path="delete-aula" element={<DeleteAula />} />
        </Route>
        <Route path="*" element={<h3>Not Found</h3>} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
